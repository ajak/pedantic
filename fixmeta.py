'''
Pedantic is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

This piece of pedantic attempts to derive what the song metadata should be
based on the filename and directory structure, then sets those metadata tags.

Known Bugs:
Year isn't being set properly. This might be a problem with FLAC files,
as mutagen seems to set the FLAC metadata tag "year" as the year rather
than the "date" tag.

The album isn't set correctly if there is a leading parentheses.
'''


import sys
import os
import re

from AudioFile import AudioFile


def is_letter(char):
    return re.search('[a-z]|[A-Z]', char)


def find_title(string):
    index = None
    string = list(string)

    for i in range(len(string)):
        if string[i] == ' ':
            if string[i:i+3] == [' ', '-', ' ']:
                index = i+3
                break
            else:
                index = i+1
                break

    return ''.join(string[index:])


def get_nonletters(string):
    flag = None
    string = list(string)

    for i in range(len(string)):
        if is_letter(string[i]):
            flag = i
            break

    return ''.join(string[:flag]).replace(' ', '')


def get_name(song):
    # Get the basename of the song path, then slice the track and file
    # extension off.
    filename = os.path.basename(song)
    song = os.path.splitext(filename)[0]

    return find_title(song)


def get_track(song):
    # Split the song path string at spaces then use the first index as the
    # track
    filename = os.path.basename(song)
    track = filename.split(' ')[0]

    # Remove '[]()' characters
    return track.translate(str.maketrans(str(), str(), '[]()'))


def get_album(song):
    # Get parent directory and slice off the year
    albumdir = os.path.abspath(os.path.join(song, ".."))
    album = os.path.basename(albumdir)

    return find_title(album)


def get_artist(song):
    # Get parent directory's parent directory
    artistdir = os.path.abspath(os.path.join(song, "../.."))

    return os.path.basename(artistdir)


def get_year(song):
    # Get parent directory of song, then slice
    albumdir = os.path.abspath(os.path.join(song, ".."))
    year = os.path.basename(albumdir)

    # Remove spaces, dashes, brackets, and parentheses
    return get_nonletters(year).translate(str.maketrans(str(),
                                                        str(),
                                                        ' -[]()'))


def fixmeta(songpath, args):
    try:
        songmeta = AudioFile(songpath)
    except:
        print("Couldn't get metadata from '" + songpath + "'")
        return None

    '''
    for when verbosity is implemented
    print("Set song to '{0}'".format(str(get_name(song))))
    print("Set track to '{0}'".format(str(get_track(song))))
    print("Set album to '{0}'".format(str(get_album(song))))
    print("Set artist to '{0}'".format(str(get_artist(song))))
    print("Set year to '{0}'".format(str(get_year(song))))
    '''

    songmeta.set_artist(args.artist if args.artist
            else get_artist(songmeta.get_path()))

    songmeta.set_year(args.year if args.year
            else get_year(songmeta.get_path()))

    songmeta.set_album(args.album if args.album
            else get_album(songmeta.get_path()))

    songmeta.set_track(args.track if args.track
            else get_track(songmeta.get_path()))

    songmeta.set_song(args.song if args.song
            else get_name(songmeta.get_path()))
