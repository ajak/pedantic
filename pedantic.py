'''
Pedantic is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


import argparse
import sys

from fixmeta import fixmeta
from fixname import fixname


legal_commands = [
    'fixmeta',
    'fixname',
]


def get_files(args):
    if args.files != list():
        return args.files
    elif not sys.stdin.isatty():  # If there's data in stdin, read it as input
        files = sys.stdin.read().split('\n')

        # Raw list has an empty element at the end, slice it off
        return files[:-1]
    else:
        print("No files given. Exiting...")
        sys.exit(1)


def main():
    # Ugly argument parsing code
    top_parser = argparse.ArgumentParser()

    subparsers = top_parser.add_subparsers(title="commands")

    fixmeta_parser = subparsers.add_parser("fixmeta")
    fixname_parser = subparsers.add_parser("fixname")

    fixmeta_parser.add_argument("--artist", default=None)
    fixmeta_parser.add_argument("--year", default=None)
    fixmeta_parser.add_argument("--album", default=None)
    fixmeta_parser.add_argument("--track", default=None)
    fixmeta_parser.add_argument("--song", default=None)

    # Especially ugly
    fixmeta_parser.add_argument("files", nargs='*')
    fixname_parser.add_argument("files", nargs='*')

    fixmeta_parser.set_defaults(func=fixmeta)
    fixname_parser.set_defaults(func=fixname)

    args = top_parser.parse_args(sys.argv[1:])

    if args.func == fixmeta or args.func == fixname:
        files = get_files(args)

    if args.func == fixmeta:
        for f in files:
            args.func(f, args)
    elif args.func == fixname:
        for f in files:
            args.func(f)
