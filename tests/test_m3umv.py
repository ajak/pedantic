'''
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


import m3umv

import unittest
import pytest


class Test_m3umv(unittest.TestCase):

    testsrc = "/.../The Beatles/1969 Abbey Road/01 The Beatles - Come Together.mp3"
    testdest = "/.../The Beatles/1969 Abbey Road/01 Come Together.mp3"

    testplaylist = \
                   ['/.../The Beatles/1969 Abbey Road/01 The Beatles - Come Together.mp3\n',
                    '/.../The Beatles/1969 Abbey Road/02 Something.mp3\n']

    resultplaylist = \
                     ['/.../The Beatles/1969 Abbey Road/01 Come Together.mp3\n',
                      '/.../The Beatles/1969 Abbey Road/02 Something.mp3\n']

    def test_fix_playlists(self):
        testresult = m3umv.fix_playlist(Test_m3umv.testsrc,
                                        Test_m3umv.testdest,
                                        Test_m3umv.testplaylist)

        assert testresult == Test_m3umv.resultplaylist
