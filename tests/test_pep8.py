'''
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import os
import unittest

import pep8
import pytest

def get_files(directory):
    for root, dirs, files in os.walk(directory):
        # Return a list of py files in directory
        return [os.path.join(root, f) for f in files
                if f.split('.')[-1] == 'py']


class TestPEP8(unittest.TestCase):

    def test_pep8(self):
        print("\n")
        result = pep8.StyleGuide().check_files(get_files("."))
        self.assertEqual(result.total_errors, 0,
                         "Code formatting issues, see above errors")
