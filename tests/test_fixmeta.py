'''
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


import fixmeta

import unittest
import pytest


class Test_fixmeta(unittest.TestCase):

    testpath_one = "/.../The Beatles/1969 Abbey Road/01 Come Together.mp3"
    testpath_two = "/.../The Beatles/1969 Abbey Road/11 Mean Mr. Mustard.mp3"

    def test_is_letter(self):
        letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

        for letter in letters:
            assert fixmeta.is_letter(letter)

    def test_trim_nonletters(self):
        test_strings = [
            "[1234] test",
            "(1234) test",
            "\][(!@ test",
            "1234 test",
            "     test"
        ]

        for string in test_strings:
            assert fixmeta.trim_nonletters(string) == "test"

    def test_get_nonletters(self):
        assert fixmeta.get_nonletters("[1234] test") == "[1234]"
        assert fixmeta.get_nonletters("(1234) test") == "(1234)"
        assert fixmeta.get_nonletters("\][(!@ test") == "\][(!@"
        assert fixmeta.get_nonletters("1234 test") == "1234"
        assert fixmeta.get_nonletters("     test") == ""

    def test_get_name(self):
        assert fixmeta.get_name(Test_fixmeta.testpath_one) == "Come Together"
        assert fixmeta.get_name(Test_fixmeta.testpath_two) == "Mean Mr. Mustard"

    def test_get_track(self):
        assert fixmeta.get_track(Test_fixmeta.testpath_one) == "01"

    def test_get_album(self):
        assert fixmeta.get_album(Test_fixmeta.testpath_one) == "Abbey Road"

    def test_get_year(self):
        assert fixmeta.get_year(Test_fixmeta.testpath_one) == "1969"
