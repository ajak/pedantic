'''
Pedantic is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Note:
If the title contains a '/' character, it will be replaced by '-'.
'''


import sys
import os

from AudioFile import AudioFile


def confirm(src, dest):
    if src == dest:
        print('Not asking to rename {0} to itself'.format(src))
        return 0

    response = input("Rename '{0}' to '{1}'? [Y/n] (Default Y): "
                     .format(src, dest)).lower()

    if response == 'y' or response == 'yes' or response == '':
        os.rename(src, dest)
        print("Renamed.")
    elif response == 'n' or response == 'no':
        print("Not renamed.")
    else:
        print("Response not understood. Got '{0}'".format(response))
        confirm(src, dest)


def fixname(filename):
    try:
        audiofile = AudioFile(filename)
    except:
        print("Couldn't get metadata from '" + filename + "'")
        return None

    track = audiofile.get_track()
    artist = audiofile.get_artist()
    title = audiofile.get_song()
    ext = audiofile.get_file_type(filename)

    # We don't want the /[totalTracks] as is in some track tags
    # If '/' is in the track string, get the index of it then use list indices
    # to exclude that part.
    if '/' in track:
        index = list(track).index('/')
        track = ''.join(list(track)[:index])

    # Preserve proper ordering of tracks in their directory by prepending a '0'
    # to single digit track numbers
    if len(track) == 1:
        track = '0' + track

    # Remove the artist's name from song title.
    title = ' '.join([word for word in title.split()
                      if artist not in word])

    # Don't prepend '/' to files in the same dir
    if '/' in filename:
        newname = os.path.dirname(filename) + "/" + track + " " + title \
                  + "." + ext
    else:
        newname = track + " " + title + "." + ext

    newname = newname.replace('/', '-')

    confirm(filename, newname)
