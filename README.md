# Usage

pedantic has three subcommands - `fixname`, `fixmeta`, and `m3umv`. Audio
formats currently supported are MP3 and FLAC.

Usage of each subcommand is as follows.

## fixname

The fixname command renames the given file(s) based on the track and song title
in their metadata. The file's name becomes '$TRACK $TITLE.$EXT'. Currently,
this command displays what the file would be renamed to and then asks the user
whether or not to rename it. If the track is a single digit, a 0 is prefixed to
it to maintain proper ordering of tracks in a directory from 0 to n.

The following example shows the ID3v2 frames containing the title and track for
'song.mp3' and renames the file based on that data.

```sh
~ $ mid3v2 -l song.mp3 | grep -E 'TIT2|TRCK' # Get the song title and track
TIT2=introducing neals
TRCK=4
~ $ pedantic fixname song.mp3
Rename 'song.mp3' to '04 introducing neals.mp3'? [Y\n] (Default Y): y
Renamed.
~ $
```

## fixmeta

The fixmeta command modifies the metadata of the given file(s) based on the
filename and the two immediate parent directories. This command assumes several
things about the file and two of its parent directories:

* The file to be operated on has the name '$TRACK $TITLE.$EXT'
* The directory containing the file to be operated on has the name
  '$YEAR $ALBUM'
* The directory containing the previously mentioned directory has the name
  '$ARTIST'.

Based on this information, fixmeta then sets the metadata tags for artist,
year, album, track number, and song title.

If this isn't accurate to the way your file structure is, you can override each
tag with these options:

* `pedantic fixmeta --artist [artist]`
* `pedantic fixmeta --year [year]`
* `pedantic fixmeta --album [album]`
* `pedantic fixmeta --track [track]`
* `pedantic fixmeta --song [song]`

## m3umv

This tool is WIP. Its use is not recommended.

# Installation

Clone the repository with something like:

```sh
~ $ git clone https://gitlab.com/ajak/pedantic.git/
```

Then enter the repo and install with pip:

```sh
~ $ cd pedantic

~/pedantic $ pip install -e .
```

# License

pedantic is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pedantic is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
