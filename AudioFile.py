'''
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


from mutagen.easyid3 import EasyID3
from mutagen.flac import FLAC
from mutagen.oggvorbis import OggVorbis


class AudioFile:

    def __init__(self, path):
        file_type = self.get_file_type(path)

        if file_type == 'flac':
            self.meta = FLAC(path)
        elif file_type == 'mp3':
            self.meta = EasyID3(path)
        elif file_type == 'ogg':
            self.meta = OggVorbis(path)
        else:
            raise RuntimeError('{0} does not point to an audio file!'
                               .format(path))

        self.path = path

    @staticmethod
    def get_file_type(path):
        return path.split('.')[-1].lower()

    def get_song(self):
        return self.meta['title'][0]

    def set_song(self, song):
        self.meta['title'] = [song]
        self.meta.save()

    def get_artist(self):
        return self.meta['artist'][0]

    def set_artist(self, artist):
        self.meta['artist'] = [artist]
        self.meta.save()

    def get_album(self):
        return self.meta['album'][0]

    def set_album(self, album):
        self.meta['album'] = [album]
        self.meta.save()

    def get_track(self):
        try:
            return self.meta['tracknumber'][0]
        except KeyError:
            print("Looks like this file has problems with its track tag")

    def set_track(self, track):
        self.meta['tracknumber'] = [track]
        self.meta.save()

    def get_path(self):
        return self.path

    def set_year(self, year):
        self.meta['date'] = [year]
        self.meta.save()


if __name__ == '__main__':
    audiofile = AudioFile(sys.argv[1])
    audiofile.set_year(2000)
